<?php

/**
 * @file
 * Contains google_document.page.inc.
 *
 * Page callback for Google Document entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Google Document templates.
 *
 * Default template: google_document.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_google_document(array &$variables) {
  // Fetch GoogleDocumentEntity Entity Object.
  $google_document = $variables['elements']['#google_document'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
