<?php

namespace Drupal\gdocs_import;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Google Document entity.
 *
 * @see \Drupal\gdocs_import\Entity\GoogleDocumentEntity.
 */
class GoogleDocumentEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  private function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\gdocs_import\Entity\GoogleDocumentEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished google document entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published google document entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit google document entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete google document entities');

      case 'import':
        return AccessResult::allowedIfHasPermission($account, 'import google document entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  private function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add google document entities');
  }

}
