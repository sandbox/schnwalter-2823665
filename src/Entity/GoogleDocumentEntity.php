<?php

namespace Drupal\gdocs_import\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\file\Entity\File;
use Drupal\user\UserInterface;

/**
 * Defines the Google Document entity.
 *
 * @ingroup gdocs_import
 *
 * @ContentEntityType(
 *   id = "google_document",
 *   label = @Translation("Google Document"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gdocs_import\GoogleDocumentEntityListBuilder",
 *     "views_data" = "Drupal\gdocs_import\Entity\GoogleDocumentEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\gdocs_import\Form\GoogleDocumentEntityForm",
 *       "add" = "Drupal\gdocs_import\Form\GoogleDocumentEntityForm",
 *       "edit" = "Drupal\gdocs_import\Form\GoogleDocumentEntityForm",
 *       "delete" = "Drupal\gdocs_import\Form\GoogleDocumentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\gdocs_import\GoogleDocumentEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gdocs_import\GoogleDocumentEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "google_document",
 *   admin_permission = "administer google document entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/google_document/{google_document}",
 *     "add-form" = "/admin/content/google_document/add",
 *     "edit-form" = "/admin/content/google_document/{google_document}/edit",
 *     "import-form" = "/admin/content/google_document/{google_document}/import",
 *     "delete-form" = "/admin/content/google_document/{google_document}/delete",
 *     "collection" = "/admin/content/google_document",
 *   },
 *   field_ui_base_route = "google_document.settings"
 * )
 */
class GoogleDocumentEntity extends ContentEntityBase implements GoogleDocumentEntityInterface {

  use EntityChangedTrait;

  /**
   * The markup fetched from Google Docs.
   * @var string|null
   */
  private $fetchedMarkup = NULL;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Google Document entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    /** @noinspection PhpUndefinedMethodInspection */
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setDescription(t('The name of the document that will be imported. This will also be used as the title for the imported article.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Google Document is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['imported'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last Imported'))
      ->setDescription(t('The time that the referenced Google Document was last imported.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['imported_target_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Imported Article'))
      ->setDescription(t('The article to be updated when importing the linked Google Document. Leave empty if you want to create a new article.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'node')
      ->setSetting('target_bundle', 'article')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * @return null|string
   */
  public function getFetchedMarkup() {
    return $this->fetchedMarkup;
  }

  /**
   * @param string $fetchedMarkup
   */
  public function setFetchedMarkup($fetchedMarkup) {
    $this->fetchedMarkup = $fetchedMarkup;
  }

}
