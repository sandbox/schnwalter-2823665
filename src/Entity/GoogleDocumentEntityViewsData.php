<?php

namespace Drupal\gdocs_import\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Google Document entities.
 */
class GoogleDocumentEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['google_document']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Google Document'),
      'help' => $this->t('The Google Document ID.'),
    );

    $data['google_document']['google_document_bulk_form'] = array(
      'title' => $this->t('Google Document operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple google documents.'),
      'field' => array(
        'id' => 'google_document_bulk_form',
      ),
    );

    return $data;
  }

}
