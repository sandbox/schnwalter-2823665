<?php

namespace Drupal\gdocs_import\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Google Document entities.
 *
 * @ingroup gdocs_import
 */
interface GoogleDocumentEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Google Document name.
   *
   * @return string
   *   Name of the Google Document.
   */
  public function getName();

  /**
   * Sets the Google Document name.
   *
   * @param string $name
   *   The Google Document name.
   *
   * @return \Drupal\gdocs_import\Entity\GoogleDocumentEntityInterface
   *   The called Google Document entity.
   */
  public function setName($name);

  /**
   * Gets the Google Document creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Google Document.
   */
  public function getCreatedTime();

  /**
   * Sets the Google Document creation timestamp.
   *
   * @param int $timestamp
   *   The Google Document creation timestamp.
   *
   * @return \Drupal\gdocs_import\Entity\GoogleDocumentEntityInterface
   *   The called Google Document entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Google Document published status indicator.
   *
   * Unpublished Google Document are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Google Document is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Google Document.
   *
   * @param bool $published
   *   TRUE to set this Google Document to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\gdocs_import\Entity\GoogleDocumentEntityInterface
   *   The called Google Document entity.
   */
  public function setPublished($published);

}
