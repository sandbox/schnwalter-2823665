<?php

namespace Drupal\gdocs_import;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\gdocs_import\Entity\GoogleDocumentEntity;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

/**
 * A service for downloading Google Document in HTML format.
 *
 * @see \Drupal\aggregator\Plugin\aggregator\fetcher\DefaultFetcher
 * @see \Drupal\dropzonejs\DropzoneJsUploadSave
 * @package Drupal\gdocs_import
 */
class GoogleDocumentFetcher {

  /**
   * The URL template for downloading Google Documents in HTML format.
   */
  const DOCUMENT_URL_TEMPLATE = 'https://docs.google.com/document/d/%s/export?format=html';

  /**
   * A regular expression for matching a Google Document ID. A valid id has a
   * length of 44 characters and contains lowercase and uppercase letters,
   * numbers, minuses or underscores.
   */
  const DOCUMENT_ID_REGEX = '@/document/d/([A-Za-z0-9_-]{44})/@';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Service for manipulating a file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Mime type guesser service.
   *
   * @var \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;
   */
  private $mimeTypeGuesser;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Interface for common file system operations.
   * @param \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface $mimetype_guesser
   *   The mime type guesser service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(
    Client $http_client,
    FileSystemInterface $file_system,
    MimeTypeGuesserInterface $mimetype_guesser,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->httpClient = $http_client;
    $this->fileSystem = $file_system;
    $this->mimeTypeGuesser = $mimetype_guesser;
    $this->logger = $logger_factory->get(self::class);
  }

  /**
   * Fetches a google document.
   *
   * TODO: Improve error/warning logs.
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return string|null
   */
  public function fetch(GoogleDocumentEntity $entity) {
    $url = $this->buildDownloadUrl($entity);

    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->get($url, [
        'headers' => [
          'Accept' => 'text/html',
        ],
      ]);

      if ($response->getStatusCode() != 200) {
        throw new \RuntimeException(sprintf('Invalid response status code "%s".', $response->getStatusCode()));
      }

      if (!$response->hasHeader('Content-Disposition')) {
        throw new \RuntimeException('A file download was not provided. Does the link point to a shared document?');
      }

      $contentType = $response->getHeader('Content-Type');
      $contentType = strtolower(reset($contentType));
      if ($contentType != 'text/html; charset=utf-8') {
        throw new \RuntimeException(sprintf('Invalid response content type "%s".', $contentType));
      }

      /** @var \GuzzleHttp\Psr7\Stream $stream */
      $stream = $response->getBody();

      /** @var string $markup */
      $markup = utf8_decode($stream->getContents());
      $entity->setFetchedMarkup($markup);

      // Validate the retrieved document.
      $this->validateFetchedMarkup($entity);

    } catch (RequestException $exception) {
      $translatedMessage = t('Failed to download the Google Document due to "%error".', [
        '%error' => $exception->getMessage()
      ]);

      drupal_set_message($translatedMessage, 'warning');
      $this->logger->warning($translatedMessage);
      return '';
    } catch (\Exception $exception) {
      drupal_set_message($exception->getMessage(), 'warning');
      $this->logger->warning($exception->getMessage());
      watchdog_exception('gdocs_import', $exception, $exception->getMessage(), [], 'warning');

      return '';
    }

    $this->processFetchedMarkup($entity);
  }

  /**
   * Returns a download URL for a Google Document in HTML format.
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return string
   */
  public function buildDownloadUrl(GoogleDocumentEntity $entity) {
    $remote_document_id = NULL;

    // Extract the document download ID
    $field = $entity->get('field_link');
    if (preg_match(self::DOCUMENT_ID_REGEX, $field->uri, $matches)) {
      $remote_document_id = array_pop($matches);
    }

    if (!isset($remote_document_id)) {
      throw new \InvalidArgumentException('The provided Google Document does not have a valid document ID.');
    }

    return sprintf(self::DOCUMENT_URL_TEMPLATE, $remote_document_id);
  }

  /**
   * Cleanup an HTML document
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   */
  private function validateFetchedMarkup($entity) {
  }

  /**
   * Cleanup an HTML document
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return string
   */
  private function processFetchedMarkup($entity) {
    static::cleanMarkup($entity);
    static::updateMarkup($entity);

    // Only keep the body content.
    $regexp = /** @lang RegExp */  '#.*<body[^>]*>(.*)</body>.*#is';
    $html_markup = preg_replace($regexp, '$1', $entity->getFetchedMarkup());
    $entity->setFetchedMarkup($html_markup);
  }

  /**
   *
   * TODO: Use https://packagist.org/packages/tijsverkoyen/css-to-inline-styles
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return string
   *
   * @throws \Exception
   */
  private function updateMarkup($entity) {
    $html = $entity->getFetchedMarkup();

    $document = new \DOMDocument('1.0', 'UTF-8');
    $internalErrors = libxml_use_internal_errors(true);
    $document->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
    libxml_use_internal_errors($internalErrors);

    // All links inside Google Documents point to a intermediate page.
    $this->processMarkupLinks($entity, $document);

    // Download and save all inline images.
    $this->processMarkupImages($entity, $document);

    $html = $document->saveHTML();

    $entity->setFetchedMarkup($html);
  }

  /**
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return mixed
   */
  private static function cleanMarkup($entity) {
    $html = $entity->getFetchedMarkup();

    // Replace all non-breaking-spaces with a normal space.
    $html = str_replace('&nbsp;', ' ', $html);

    // Remove inline style attributes.
    $html = preg_replace('/\ style="[^"]+"/', '', $html);

    // Cleanup document from dummy markup.
    // TODO: Do not allow importing of documents with this type of markup.
    // <p class="c2 c7"><span class="c0">Bold</span></p>
    $regex = /** @lang RegExp */ '<p class="[^"]+"><span class="[^"]+">Bold</span></p>';
    $html = preg_replace("@$regex@", '', $html);
    // <p class="c2 c7"><span class="c1 c6">Italic</span></p>
    $regex = /** @lang RegExp */ '<p class="[^"]+"><span class="[^"]+">Italic</span></p>';
    $html = preg_replace("@$regex@", '', $html);
    // <p class="c2 c7"><span class="c1 c9">Underline</span></p>
    $regex = /** @lang RegExp */ '<p class="[^"]+"><span class="[^"]+">Underline</span></p>';
    $html = preg_replace("@$regex@", '', $html);

    // Remove extra spans around image, image and anchor tags.
    $html = preg_replace('#(\ *)<span[^<]*>\ *(<a [^>]+>[^<]+</a>)\ *</span>(\ *)#', '$1$2$3', $html);
    $html = preg_replace('#(\ *)<span[^<]*>\ *(<iframe [^>]+>[^<]+</iframe>)(\ *)</span>\ *#', '$1$2$3', $html);
    $html = preg_replace('#(\ *)<span[^<]*>\ *(<img[^>]+>)(\ *)</span>\ *#', '$1$2$3', $html);

    // Extract styles in order to process them.
    $matches = [];
    $styles = '';
    if (preg_match('#<style[^>]+>([^<]*)</style>#', $html, $matches)) {
      // We add a whitespace character for our word boundaries to work.
      $styles = array_pop($matches);

      // Remove the styles tag(s) from the document.
      $html = preg_replace('#<style[^>]+>[^<]*</style>#', '', $html);

      // Remove various unneeded style declarations.
      $styles = preg_replace('/(color|content|counter-reset|font-family|font-size|line-height|orphans|text-indent|widows):[^;}]+;?/i', '', $styles);
      $styles = preg_replace('/(background|list-style|margin|padding|page-break)[a-z-]*?:[^;}]+;?/i', '', $styles);

      // Remove declarations with default values.
      $styles = preg_replace('/font-weight:\s*400;?/i', '', $styles);
      $styles = preg_replace('/text-decoration:\s*inherit;?/i', '', $styles);

      // Complete remove style declarations for certain selectors.
      $styles = preg_replace('/\b(?:p|ul|ol|li|table)\s*{[^}]*}/', '', $styles);
      $styles = preg_replace('/\bh[1-6]\s*{[^}]+}/i', '', $styles);
      $styles = preg_replace('/\.(?:sub)?title\s*{[^}]+}/i', '', $styles);
    }

    if ($styles) {
      $cssToInlineStyles = new CssToInlineStyles();
      $html = $cssToInlineStyles->convert($html, $styles);
    }

    // Remove HTML class and id attributes.
    $html = preg_replace('/\ class="[^"]+"/', '', $html);
    $html = preg_replace('/\ id="[^"]+"/', '', $html);

    // Update text formatting.
    // TODO: Deal with cases where multiple styles apply (i.e. bold & italic).
    // NOTE: The CssToInlineStyles library always creates inline styles this sway.
    $html = preg_replace('#(\ *)<span style="font-weight: 700;">\ *([^<]+?)(\ *)</span>\ *#', '$1<b>$2</b>$3', $html);
    $html = preg_replace('#(\ *)<span style="font-style: italic;">\ *([^<]+?)\ *</span>(\ *)#', '$1<i>$2</i>$3', $html);
    $html = preg_replace('#(\ *)<span style="text-decoration: underline;">\ *([^<]+?)\ *</span>(\ *)#', '$1<u>$2</u>$3', $html);

    // Remove extra spans.
    $html = preg_replace('#(\ *)<span[^<]*>([^<]+)?</span>(\ *)#i', '$1$2$3', $html);

    // Add some extra spacing between block level elements.
    $block_level_elements = [
      'div',
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'iframe',
      'object',
      'table',
      'ul',
      'ol',
    ];
    foreach ($block_level_elements as $tag) {
      $html = str_replace("<$tag>", "\n\n\n<$tag>", $html);
      // $html = str_replace("</$tag>", "</$tag>\n\n", $html);
    }

    // Remove remaining inline style attributes.
    $html = preg_replace('/\ style="[^"]+"/', '', $html);

    // Add extra whitespace around DC specific tags.
    $html = preg_replace('#\n*<p>\s*(\[DIGITAL-CITIZEN-[\w-]+\])\s*</p>\n*#', "\n\n\n<p>$1</p>\n\n\n", $html);

    // Convert and remove extra white-space.
    $html = str_replace("\r", "\n", $html);
    $html = str_replace("\n\n", "\n", $html);
    $html = str_replace("\t", " ", $html);
    $html = str_replace("  ", " ", $html);

    $entity->setFetchedMarkup($html);
  }

  /**
   * Update URLs to remove intermediate Google page.
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @param \DOMDocument $document
   */
  private function processMarkupLinks(GoogleDocumentEntity $entity, \DOMDocument $document) {
    /** @var \DOMElement $domElement */
    foreach ($document->getElementsByTagName('a') as $domElement) {
      $url = $domElement->getAttribute('href');
      if (empty($url)) {
        continue;
      }

      $parts = UrlHelper::parse($url);
      if (preg_match('#https?://www.google.com/url#i', $parts['path'])) {
        $real_path = $parts['query']['q'];
        $domElement->setAttribute('href', $real_path);
      }
    }
  }

  /**
   * Download images and save them to the files system.
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @param \DOMDocument $document
   * @throws \Exception
   */
  private function processMarkupImages(GoogleDocumentEntity $entity, \DOMDocument $document) {
    $destination = 'public://gdrive/' . $entity->get('field_images_group')->value;

    // Get all files from default standard file dir.
    $dir = \Drupal::state()->get('file_public_path') ?: 'sites/default/files';

    // Go through each one and replace this with a proper uri.
    $existing_files = array();
    foreach (file_scan_directory($destination, '(.*?)') as $existing_file_path) {
      $existing_files[] = str_replace($dir . '/', 'public://', $existing_file_path->uri);
    }

    if (!empty($existing_files)) {
      $existing_managed_files = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['uri' => $existing_files]);

      // If there are no managed files, delete the whole directory.
      if (empty($existing_managed_files)) {
        file_unmanaged_delete_recursive($destination);
      }

      // Remove managed files from the existing files list.
      $existing_managed_files_list = [];
      foreach (array_keys($existing_managed_files) as $key) {
        $fileEntity = $existing_managed_files[$key];
        /** @var \Drupal\file\Entity\File $fileEntity */
        $existing_managed_files_list[] = $fileEntity->getFileUri();
        $fileEntity->delete();
        unset($existing_managed_files[$key]);
      }

      $existing_unmanaged_files = array_diff($existing_files, $existing_managed_files_list);
      foreach($existing_unmanaged_files as $file) {
        file_unmanaged_delete($file);
      }
    }

    // Create an empty directory if it does not exist.
    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      throw new \Exception("Unable to create directory $destination.");
    }

    // Download and save all inline images.
    $delta = 1;
    $fileNamePrefix = $entity->get('field_images_filename_prefix')->value;
    /** @var \DOMElement $domElement */
    foreach ($document->getElementsByTagName('img') as $domElement) {
      $fetchedImage = NULL;
      $url = $domElement->getAttribute('src');
      try {
        $tempDestination = $this->fileSystem->tempnam('temporary://', 'GoogleDocumentFetcher_');
        $tempUri = system_retrieve_file($url, $tempDestination, FALSE, FILE_EXISTS_REPLACE);

        // TODO: Use the mime_type_guesser service.
        // $mimeType = $this->mimeTypeGuesser->guess($tempPath);
        $tempPath = $this->fileSystem->realpath($tempUri);
        $imageInfo = getimagesize($tempPath);

        if (empty($imageInfo['mime'])) {
          throw new \RuntimeException(sprintf('Could not determine the mime-type for downloaded file %s', $url));
        }

        $fileMime = strtolower($imageInfo['mime']);
        switch ($fileMime) {
          case 'image/jpeg':
            $fileName = $fileNamePrefix . '_' . $delta . '.jpg';
            break;
          case 'image/png':
            $fileName = $fileNamePrefix . '_' . $delta . '.png';
            break;
          default:
            throw new \RuntimeException(sprintf('The downloaded file %s does not have an accepted mime-type %s', $url, $imageInfo['mime']));
        }

        $finalPath = $destination . '/' . $fileName;
        file_unmanaged_move($tempPath, $finalPath, FILE_EXISTS_REPLACE);

        // TODO: Validate the file using the destination field validators.
        $fetchedImage = File::create([
          'filename' => $fileName,
          'uri' => $destination . '/' . $fileName,
          'filemime' => $fileMime,
        ]);
        $fetchedImage->save();
      } catch (\Exception $exception) {

      }

      if ($fetchedImage) {
        // Update the image source attribute.
        $domElement->setAttribute('src', file_create_url($fetchedImage->getFileUri()));

        // Update the image alternative text attribute.
        // TODO: Use a special token for adding description for each image.
        $domElement->setAttribute('alt', $entity->get('field_images_description')->value);

        // Remove the empty title attribute.
        $domElement->removeAttribute('title');

        // Add embedded entity integration.
        $domElement->setAttribute('data-entity-type', 'file');
        $domElement->setAttribute('data-entity-uuid', $fetchedImage->uuid());
      }

      $delta++;
    }

  }
}
