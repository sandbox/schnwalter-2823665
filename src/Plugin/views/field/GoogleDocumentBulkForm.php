<?php

namespace Drupal\gdocs_import\Plugin\views\field;

use Drupal\system\Plugin\views\field\BulkForm;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("google_document_bulk_form")
 */
class GoogleDocumentBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  private function emptySelectedMessage() {
    return $this->t('No content selected.');
  }

}
