<?php

namespace Drupal\gdocs_import;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Google Document entities.
 *
 * @ingroup gdocs_import
 */
class GoogleDocumentEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Google Document ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gdocs_import\Entity\GoogleDocumentEntity */
    $row['id'] = $entity->id();
    $row['name'] = $entity->toLink();
    return $row + parent::buildRow($entity);
  }


  private function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('import') && $entity->hasLinkTemplate('import-form')) {
      $operations['import'] = array(
        'title' => $this->t('Import'),
        'weight' => 100,
        'url' => $entity->toUrl('import-form'),
      );
    }

    return $operations;
  }
}
