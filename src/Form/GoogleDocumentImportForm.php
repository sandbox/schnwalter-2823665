<?php

namespace Drupal\gdocs_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gdocs_import\Entity\GoogleDocumentEntity;
use Drupal\gdocs_import\GoogleDocumentFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoogleDocumentImportForm.
 *
 * @package Drupal\gdocs_import\Form
 */
class GoogleDocumentImportForm extends FormBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The Google Document fetcher service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $fetcher;

  /**
   * GoogleDocumentImportForm constructor.
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\gdocs_import\GoogleDocumentFetcher $fetcher
   */
  public function __construct(Request $request, GoogleDocumentFetcher $fetcher) {
    $this->request = $request;
    $this->fetcher = $fetcher;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(), $container->get('gdocs_import.fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_document_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load the current Google Document entity..
    /** @var \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity */
    $entity = $this->request->get('google_document');

    /** @var null|\Drupal\node\Entity\Node $importedTargetEntity */
    $importedTargetEntity = NULL;
    if (!$entity->get('imported_target_id')->isEmpty()) {
      $importedTargetEntity = $entity->get('imported_target_id')->entity;
    }

    $documentUrlIsValid = FALSE;
    try {
      $documentUrlIsValid = (bool) $this->fetcher->buildDownloadUrl($entity);
    } catch (\InvalidArgumentException $exception) {
      drupal_set_message($exception->getMessage(), 'error');
    }

    $form['information'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Information'),
      '#collapsible' => FALSE,
    ];

    if (empty($importedTargetEntity)) {
      $form['information']['#description'] = $this->t('A new article will be created.');
    }
    else {
      $form['information']['#description'] = $this->t('The article that will be updated.');

      $form['information']['link'] = [
        '#type' => 'link',
        '#title' => $importedTargetEntity->label(),
        '#url' => $importedTargetEntity->toUrl('canonical', ['attributes' => ['target' => '_blank']]),
        '#description' => $this->t('label'),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $importedTargetEntity ? $this->t('Import and update existing article') : $this->t(
        'Import and create new article'
      ),
      '#disabled' => empty($documentUrlIsValid),
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $entity->toUrl(),
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load the current Google Document entity..
    /** @var \Drupal\gdocs_import\Entity\GoogleDocumentEntity $googleDocumentEntity */
    $googleDocumentEntity = $this->request->get('google_document');
    $this->fetcher->fetch($googleDocumentEntity);

    if ($googleDocumentEntity->get('imported_target_id')->isEmpty()) {
      /** @var \Drupal\node\Entity\Node $importedTargetEntity */
      $importedTargetEntity = $this->createNewArticleNode($googleDocumentEntity);
      $importedTargetEntity->save();
    }
    else {
      /** @var \Drupal\node\Entity\Node $imported_target_entity */
      $importedTargetEntity = $googleDocumentEntity->get('imported_target_id')->entity;
    }

    // Update the article title.
    $importedTargetEntity->get('title')->setValue($googleDocumentEntity->getName());

    // Update the article body.
    $importedTargetEntity->get('body')->setValue(
      [
        'value' => $googleDocumentEntity->getFetchedMarkup(),
        'summary' => '',
        'format' => 'restricted_html',
      ]
    );

    // Update the article real author.
    $importedTargetEntity->setOwnerId($googleDocumentEntity->get('field_real_author')->target_id);

    // TODO: Only save after files have been attached. (Use tokens inside the body field).
    $importedTargetEntity->save();

    // Update the Google Document entity.
    $googleDocumentEntity->set('imported', REQUEST_TIME);
    $googleDocumentEntity->get('imported_target_id')->appendItem(['target_id' => $importedTargetEntity->id()]);
    $googleDocumentEntity->save();

    if ($googleDocumentEntity->get('imported_target_id')->isEmpty()) {
      $context = [
        '@type' => node_get_type_label($importedTargetEntity),
        '%title' => $importedTargetEntity->label(),
      ];
      $this->logger('content')->notice('@type: added %title.', $context);

      $t_args = [
        '@type' => node_get_type_label($importedTargetEntity),
        '%title' => $importedTargetEntity->link($importedTargetEntity->label()),
        '%edit' => $importedTargetEntity->link($this->t('edit'), 'edit-form'),
      ];
      drupal_set_message(t('@type %title has been created. Go to the %edit form.', $t_args));
    }
    else {
      $context = [
        '@type' => $importedTargetEntity->getType(),
        '%title' => $importedTargetEntity->label(),
      ];
      $this->logger('content')->notice('@type: updated %title.', $context);

      $t_args = [
        '@type' => node_get_type_label($importedTargetEntity),
        '%title' => $importedTargetEntity->link($importedTargetEntity->label()),
        '%edit' => $importedTargetEntity->link($this->t('edit'), 'edit-form'),
      ];
      drupal_set_message(t('@type %title has been updated. Go to the %edit form.', $t_args));
    }
  }

  /**
   * Creates a new Article.
   *
   * @param \Drupal\gdocs_import\Entity\GoogleDocumentEntity $entity
   * @return \Drupal\node\Entity\Node
   */
  function createNewArticleNode(GoogleDocumentEntity $entity) {
    $values = [
      'type' => 'article',
      'field_article_type' => $entity->get('field_article_type')->value,
      'title' => $entity->getName(),
    ];

    /** @var \Drupal\node\Entity\Node $newArticle */
    $newArticle = \Drupal::entityTypeManager()->getStorage('node')->create($values);

    if (!$newArticle->validate()) {
      throw new \RuntimeException(sprintf('The created article entity is not valid.'));
    }

    return $newArticle;
  }
}
