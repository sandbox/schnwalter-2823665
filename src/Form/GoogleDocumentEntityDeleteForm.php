<?php

namespace Drupal\gdocs_import\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Google Document entities.
 *
 * @ingroup gdocs_import
 */
class GoogleDocumentEntityDeleteForm extends ContentEntityDeleteForm {


}
