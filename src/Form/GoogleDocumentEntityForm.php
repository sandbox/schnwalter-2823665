<?php

namespace Drupal\gdocs_import\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Google Document edit forms.
 *
 * @ingroup gdocs_import
 */
class GoogleDocumentEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /* @var $entity \Drupal\gdocs_import\Entity\GoogleDocumentEntity */
    $entity = $this->entity;

    /** @noinspection PhpUndefinedFieldInspection */
    if ($entity->imported_target_id->entity && isset($form['imported_target_id']['widget'][0]['target_id'])) {
      // Lock the imported_target_id once it has been set.
      $form['imported_target_id']['#disabled'] = TRUE;

      /** @noinspection PhpUndefinedFieldInspection */
      $articleUrl = $entity->imported_target_id->entity->toUrl()->toString();
      $fieldSuffix = t('View <a href="@url" target="_blank">article</a>.', array('@url' => $articleUrl));
      $form['imported_target_id']['widget'][0]['target_id']['#field_suffix'] = $fieldSuffix;

      $fieldDescription = $this->t('The article to be updated when importing the linked Google Document.');
      $form['imported_target_id']['widget'][0]['target_id']['#description'] = $fieldDescription;
    }

    if (isset($form['field_link']['widget'][0]['uri'])) {
      $fieldDescription = $this->t('The link to a Google Document that is publicly accessible.');
      $form['field_link']['widget'][0]['uri']['#description'] = $fieldDescription;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  private function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);

    // Convert the "Save" button to a dropbutton.
    $element['submit']['#value'] = $this->t('Save and go document details page');
    $element['submit']['#dropbutton'] = 'save';

    $element['submit_import'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save and go article import page'),
      '#submit' => array('::submitForm', '::save'),
      '#dropbutton' => 'save',
      '#destination_route' => 'entity.google_document.import_form',
      '#weight' => -10,
    );

    // Always keep the delete link at the end.
    if (isset($element['delete'])) {
      $element['delete']['#weight'] = 100;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Google Document.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Google Document.', [
          '%label' => $entity->label(),
        ]));
    }

    // Redirect tot eh proper destination page.
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#destination_route'])) {
      $form_state->setRedirect($triggering_element['#destination_route'], ['google_document' => $entity->id()]);
    }
    else {
      // Fallback: Go to the entity page.
      $form_state->setRedirect('entity.google_document.canonical', ['google_document' => $entity->id()]);
    }
  }

}
